// Owner: M3r

#pragma once

#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "Runtime/Core/Public/Containers/Array.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

    void OpenDoor();

    void CloseDoor();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


    UPROPERTY(EditAnywhere)
    ATriggerVolume* pressurePlateTrigger = nullptr;

    UPROPERTY(EditAnywhere)
    AActor* pressurePlateMesh = nullptr;

    /// Moved to BP
    //UPROPERTY(EditAnywhere)
    //float openAngle = -60.f;

    /// Moved to BP
    //UPROPERTY(EditAnywhere)
    //float timeAfterDoorCloses = 0.7f;

    UPROPERTY(BlueprintAssignable)
    FDoorEvent onOpen;

    UPROPERTY(BlueprintAssignable)
    FDoorEvent onClose;

    UPROPERTY(EditAnywhere)
    float triggerMass = 30.f;

private:
    /// Moved to BP. Controlled by the timeline.
    //float lastTimeWhenDoorOpened;

private:
    float GetTotalMassOfActorsOnPressurePlate();
};
