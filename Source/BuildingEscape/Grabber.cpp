// Owner: M3r

#include "Grabber.h"

#define OUT

UGrabber::UGrabber()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

    FindPhysicsHandle();

    FindInputController();
}

void UGrabber::FindInputController()
{
    pawnInputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
    if (pawnInputComponent)
    {
        UE_LOG(LogTemp, Warning, TEXT("Input controller found."));

        pawnInputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
        pawnInputComponent->BindAction("Grab", IE_Released, this, &UGrabber::ReleaseGrab);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Input controller component has not been found for %s."), *(GetOwner()->GetName()));
    }
}

void UGrabber::FindPhysicsHandle()
{
    physicsHandleComponent = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
    if (!physicsHandleComponent)
    {
        UE_LOG(LogTemp, Error, TEXT("Physics handle component has not been found for %s."), *(GetOwner()->GetName()));
    }
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (!physicsHandleComponent) { return; }
    if (physicsHandleComponent->GrabbedComponent)
    {
        physicsHandleComponent->SetTargetLocation(GetLineTracePoints().v2);
    }
}

void UGrabber::Grab()
{
    auto hitResult = GetFirstPhysicsBodyInReach();
    auto componentToGrab = hitResult.GetComponent();
    auto actorHit = hitResult.GetActor();

    if (!physicsHandleComponent) { return; }
    if (actorHit)
    {
        physicsHandleComponent->GrabComponentAtLocationWithRotation(
            componentToGrab,
            NAME_None,
            componentToGrab->GetOwner()->GetActorLocation(),
            FRotator(0.f, 0.f, 0.f)
        );
    }
}

void UGrabber::ReleaseGrab()
{
    if (!physicsHandleComponent) { return; }
    physicsHandleComponent->ReleaseComponent();
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
    FHitResult hitResult;
    FCollisionQueryParams traceParameters(FName(TEXT("")), false, GetOwner());
    GetWorld()->LineTraceSingleByObjectType(
        OUT hitResult,
        GetLineTracePoints().v1, // start line point
        GetLineTracePoints().v2, // end line point
        FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
        traceParameters
    );

    return hitResult;
}

FTwoVectors UGrabber::GetLineTracePoints()
{
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
        OUT viewPointPosition,
        OUT viewPointRotation
    );

    return FTwoVectors(viewPointPosition, viewPointPosition + viewPointRotation.Vector() * reach);
}
