// Owner: M3r

#include "OpenDoor.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

    if (!pressurePlateTrigger || !pressurePlateMesh)
    {
        if (!pressurePlateTrigger)
        {
            UE_LOG(LogTemp, Error, TEXT("Pressure Plate Trigger has not been set for %s."), *(GetOwner()->GetName()));
        }

        if (!pressurePlateMesh)
        {
            UE_LOG(LogTemp, Error, TEXT("Pressure Plate Mesh has not been set for %s."), *(GetOwner()->GetName()));
        }
        return;
    }
}

void UOpenDoor::OpenDoor()
{
    // Old system. Now controlled by BP.
    // GetOwner()->SetActorRotation(FRotator(0.f, openAngle, 0.f)); 

    // Broadcasting a request to open door for its BP
    onOpen.Broadcast();

    if (!pressurePlateMesh) { return; }
    pressurePlateMesh->SetActorScale3D(FVector(1.f, 1.f, 0.01f));
}

void UOpenDoor::CloseDoor()
{
    onClose.Broadcast();

    if (!pressurePlateMesh) { return; }
    pressurePlateMesh->SetActorScale3D(FVector(1.f, 1.f, 0.1f));
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    
    if (pressurePlateTrigger && pressurePlateMesh && GetTotalMassOfActorsOnPressurePlate() > triggerMass)
    {
        OpenDoor();

        // Old system. Now controlled by BP.
        //lastTimeWhenDoorOpened = GetWorld()->GetTimeSeconds();
    }
    else
    {
        CloseDoor();
    }

    // Old system. Now controlled by BP.
    /* 
    if (lastTimeWhenDoorOpened && GetWorld()->GetTimeSeconds() - lastTimeWhenDoorOpened > timeAfterDoorCloses)
    {
        CloseDoor();
        lastTimeWhenDoorOpened = 0;
    }
    */
}

float UOpenDoor::GetTotalMassOfActorsOnPressurePlate()
{
    float totalMass = 0;

    TArray<AActor*> overlappingActors;

    // find all overlapping actors
    if (!pressurePlateTrigger) { return 0; }
    pressurePlateTrigger->GetOverlappingActors(OUT overlappingActors);

    /// get their mass and store it
    for (auto* actor : overlappingActors)
    {
        totalMass = totalMass + actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
    }

    // return mass
    return totalMass;
}

