// Owner: M3r

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsHandleComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    FVector viewPointPosition;
    FRotator viewPointRotation;

    float reach = 150.f;

    UPhysicsHandleComponent* physicsHandleComponent = nullptr;

    UInputComponent* pawnInputComponent = nullptr;

private:
    void FindInputController();

    void FindPhysicsHandle();

    void Grab();
    void ReleaseGrab();

    FHitResult GetFirstPhysicsBodyInReach();

    FTwoVectors GetLineTracePoints();
};
